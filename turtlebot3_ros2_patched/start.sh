# Source the patched packages
. /${PACKAGE_NAME}/hls_lfcd_lds_driver/install/local_setup.sh

# If desired, update `OpenCR1.0`
if [ "${UPDATE_OPENCR}" = "true" ]; then
    cd opencr_update
    ./update.sh "${OPENCR_PORT}" "${TURTLEBOT3_MODEL}".opencr
    cd ..
fi

# If desired, set `ROS_DOMAIN_ID` according to `HOSTNAME` but keep only digits
if [ "${ROS_DOMAIN_ID_BASED_ON_HOSTNAME}" = "true" ]; then
    # Export with only digits
    export ROS_DOMAIN_ID=$(echo "${HOSTNAME}" | tr -cd '[[:digit:]]')

    # Make sure the `ROS_DOMAIN_ID` is valid
    if [[ "${ROS_DOMAIN_ID}" -lt "0" || "${ROS_DOMAIN_ID}" -gt "232" ]]; then
        echo "ERROR: \`ROS_DOMAIN_ID\` must be in interval [0, 232]"
        exit 1
    fi
fi

# Launch `turtlebot3`
ros2 launch turtlebot3_bringup robot.launch.py
